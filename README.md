database : no*

language : php.

front :

- index.php:
    - formulaire :
        - Input = Vemprunt (number); annees (number); Tinteret (number) %;
        - Select (type) = annuite constante ⇒ value = 1 / degressif ⇒ value = 2.
        - Button action ⇒ traitement.php
        
        <aside>
        💡 Each input must be filled !       
        </aside>
        
- result.php:
    - table [ Annees, Emprunt restant Interet, Amortissement, Annuite, Valeur nette ]
    - button to go back to index.php.

Traitement :

- traitement.php :
    - Verify if the all if Vemprunt, annees, Tinterest are not empty
    - check type :
        - if 1 function annuite_constante()
        - else function annuite_degressif()
    - function annuite_constante():
    
    L’annuite ne change pas au file des calculs mais l’amortissement change
    
    - Annuité Formule : annuite = C x **i /** 1-(1 + i) -n
    - Interet = Vemprunt x (Tinteret / 100)
    amortissement = Annuité - Interet
    V_nette = Vemprunt – amortissement.
    - Vemprunt = V_nette
    
    should return array[nb_annee; Vemprunt; Interet; amortissement; annuite; V_nette]
    
    - function annuite_degressif():
    
    L’annuite change au file des calculs en annee mais pas l’amortissement.
    
    - Amortissement = Vemprunt/annees.
    Intérêt = Vemprunt x Tinteret
    Annuité = Amortissement + Intérêt
    V_nette = Vemprunt – Amortissement de l’année
    - Vemprunt = V_nette
    
    should return array[nb_annee; Vemprunt; Interet; amortissement; annuite; V_nette]
    

Integration :

link traitement & front and make the table generate the result typed in index.php.